FROM golang:1.11-alpine3.9  as build

WORKDIR /go/src/app

COPY . .

RUN go build -o app

FROM alpine:3.9

COPY --from=build /go/src/app/app /usr/local/bin/app

ENTRYPOINT ["/usr/local/bin/app"]
